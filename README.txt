PCAP Analyzer and HTTP Classifier

=== ABOUT =====
-------------------------------------------------------------------------------
Tool to report TCP/HTTP flows parsed to display per packet and flow-wise data
like retransmissions, congestion windows, throughput, RTT, PLT and loss along
with a linear classifier for HTTP version.

=== USAGE =====
-------------------------------------------------------------------------------
TCP Processing:

[USAGE]: python3 analysis_pcap_tcp.py <path to pcap file>
[EXAMPLE]: python3 analysis_pcap_tcp.py <assignment2.pcap>

-------------------------------------------------------------------------------
HTTP Processing:

Analyzer:
[USAGE]: python3 analysis_pcap_http.py <path to pcap file>
[EXAMPLE]: python3 analysis_pcap_http.py <http_8092.pcap>

Classifier:
[USAGE]: python3 HTTP_version_classifier.py <path to pcap file> <server_port>
[EXAMPLE]: python3 HTTP_version_classifier.py <http_8093.pcap> <8093>
-------------------------------------------------------------------------------

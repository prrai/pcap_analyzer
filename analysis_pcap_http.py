import logging
import sys
import re
import codecs
from collections import defaultdict
import binascii

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import PcapReader

server_port = 8092


class Packet:
    def __init__(self):
        self.frame_number = ''
        self.packet_size = ''
        self.relative_arr_time = ''
        self.source_ip = ''
        self.dest_ip = ''
        self.source_port = ''
        self.dest_port = ''
        self.seq_number = ''
        self.ack_number = ''
        self.window_size = ''
        self.tcp_payload_len = ''
        self.tcp_header_len = ''
        self.flags = []
        self.http_mode = None
        self.http_header = None

class PCAPAnalyzer:
    def __init__(self):
        self.packets = []
        self.flows = []
        self.pcap_file_path = ''
        self.validate_args_and_populate_packets()
        pass

    def validate_args_and_populate_packets(self):
        if len(sys.argv) == 2:
            if sys.argv[1] in {"--help", "--HELP", "--h", "--H", "-h", "-H", "-help", "-HELP"}:
                self._display_usage_and_exit()
            self.pcap_file_path = sys.argv[1]
            self._read_pcap_and_fill_packets()
        else:
            print("Incomplete/Improper command line arguments supplied.")
            self._display_usage_and_exit()

    def display_http_header(self, type, header_bytes):
        """
        Function to display http information
        :param type:
        :param header_bytes:
        :return:
        """
        # Stream the packet bytes to string
        content = binascii.unhexlify(header_bytes)
        content = str(content).split('\'')[1].split('\'')[0].split("\\r\\n")
        print("###################################################")
        if type == 'req':
            print("========== HTTP Request Header: BEGIN =============")
            for item in content:
                print(item)
            print("========== HTTP Request Header: END ===============")
        if type == 'res':
            print("========= HTTP Response Header: BEGIN =============")
            for item in content:
                print(item)
            print("========= HTTP Response Header: END ===============")
        print("###################################################")

    def find_total_HTTP_flows(self):
        """
        Function to get the total HTTP flows in the trace
        :return:
        """
        syn_requests = []
        for i in range(0, len(self.packets)):
            if 'SYN' in self.packets[i].flags and self.packets[i].dest_port == server_port:
                syn_requests.append(self.packets[i])
        print("Total HTTP transactions in the trace: {0}".format(len(syn_requests)))
        self.flows = self._get_flows(syn_requests)
        for i in range(0, len(self.flows)):
            print("Total packets exchanged in Flow #{0} : {1}".format(i + 1, len(self.flows[i])))
        print("\nDisplaying details for each flow:")
        ctr = 1
        for i in range(0, len(self.flows)):
            http_request_packet = Packet()
            http_response_packet = Packet()
            for j in range(0, len(self.flows[i])):
                if self.flows[i][j].http_mode == 'req':
                    http_request_packet = self.flows[i][j]
                    break
            for j in range(0, len(self.flows[i])):
                if self.flows[i][j].http_mode == 'res':
                    http_response_packet = self.flows[i][j]
                    break
            print("\n\n============== HTTP FLOW #{0}: BEGIN ===============".format(ctr))
            print("\nHTTP REQUEST PACKET FROM CLIENT TO SERVER:")
            self.display_http_header('req', http_request_packet.http_header)
            print("\nHTTP RESPONSE PACKET FROM  SERVER TO CLIENT:")
            self.display_http_header('res', http_response_packet.http_header)
            print("\nNow print the entire data exchange:")
            for j in range(0, len(self.flows[i])):
                self.display_packet_info(self.flows[i][j])
                if self.flows[i][j].http_mode is not None:
                    self.display_http_header(self.flows[i][j].http_mode, http_request_packet.http_header)
            print("=============== HTTP FLOW #{0}: END ================".format(ctr))
            ctr += 1

    def estimate_loss_rate(self):
        print("\n======= LOSS RATE CALCULATIONS PER FLOW =========")
        for i in range(0, len(self.flows)):
            sent_count = 0
            print("Estimating loss rate (SERVER SIDE) for FLOW# {0}".format(i + 1))
            print("**************************************************")
            sender_port = self.flows[i][0].dest_port
            print("Losses => Retransmitted packets since original ones didn't get acknowledged in time")
            sender_retrans_dict = defaultdict(int)
            for k in range(0, len(self.flows[i])):
                if self.flows[i][k].source_port == sender_port:
                    sender_retrans_dict[(self.flows[i][k].seq_number, self.flows[i][k].tcp_payload_len,
                                         self.flows[i][k].ack_number)] += 1
                    sent_count += 1
            retransmits = 0
            for key in sender_retrans_dict:
                retransmits += sender_retrans_dict[key]
            retransmits -= len(sender_retrans_dict)
            print("Total retransmissions are: {0}".format(retransmits))
            print("Total sent packets from source are: {0}".format(sent_count))
            print("Loss rate = (retransmits / sent packets)")
            print("_________________________________________________________________________")
            print("Loss rate % = {0}".format(100 * round(float(retransmits / sent_count), 6)))
            print("_________________________________________________________________________")
            print("**************************************************\n")

    def find_throughput(self):
        print("\n======= EMPIRICAL THROUGHPUT CALCULATIONS (SERVER TO CLIENT) PER FLOW =========")
        for i in range(0, len(self.flows)):
            print("Estimating throughput for FLOW# {0}".format(i + 1))
            print("**************************************************")
            bytes_sent_to_receiver = 0
            first_source_packet = self.flows[i][0]
            for packet in self.flows[i]:
                if packet.source_port == first_source_packet.dest_port:
                    bytes_sent_to_receiver += packet.packet_size
            bytes_sent_to_receiver *= 8
            print("Total Bits sent for FLOW {0}, from server: {1} on port: {2} is: {3}"
                  .format(i + 1, first_source_packet.dest_ip, first_source_packet.dest_port,
                          bytes_sent_to_receiver))
            print("Total time spent = Handshake Time + Time spent in data exchange (all sent bytes acknowledged) "
                  "+ Tear down time")
            time_spent = round(float(self.flows[i][-1].relative_arr_time) -
                               float(self.flows[i][0].relative_arr_time), 6)
            print("Total Time: {0}".format(time_spent))
            print("Empirical throughput = (Total Bits sent) / (Total Time)")
            throughput = round(float(bytes_sent_to_receiver / time_spent), 6)
            kbps = round(float(throughput / 8 / 1024), 6)
            print("_________________________________________________________________________")
            print("Hence, Throughput = {0} Bits/sec => {1} Kilobytes/sec".format(throughput, kbps))
            print("_________________________________________________________________________")
            print("**************************************************\n")

    # Protected methods

    def _display_usage_and_exit(self):
        print("\n[USAGE]: python3 analysis_pcap_http.py <path to pcap file>")
        print("[EXAMPLE]: python3 analysis_pcap_http.py <http_8092.pcap>")
        sys.exit(2)

    def _read_pcap_and_fill_packets(self):
        try:
            with PcapReader(self.pcap_file_path) as pcap_data:
                time_ref = 0
                for packet in pcap_data:
                    if len(self.packets) == 0:
                        time_ref = packet.time
                    hex_tokens = self._serialize_packet_to_hex_tokens(packet)
                    packet_obj = Packet()
                    packet_obj.packet_size = len(packet)
                    packet_obj.frame_number = int(len(self.packets) + 1)
                    packet_obj.relative_arr_time = round(float(packet.time - time_ref), 6)
                    packet_obj = self._extract_packet_info(packet_obj, hex_tokens)
                    self.packets.append(packet_obj)
        except EnvironmentError:
            print("\n[ERROR] Unable to read input file, or it does not exist. Aborting!\n")
            self._display_usage_and_exit()

    def _extract_packet_info(self, packet_obj, hex_tokens):
        tcp_header_start_byte = self._get_tcp_header_start_byte(hex_tokens)
        packet_obj.tcp_header_len, packet_obj.tcp_payload_len = self._get_tcp_len(hex_tokens)
        packet_obj.source_port = self._get_source_port(hex_tokens, tcp_header_start_byte)
        packet_obj.dest_port = self._get_destination_port(hex_tokens, tcp_header_start_byte)
        packet_obj.seq_number = self._get_sequence_number(hex_tokens, tcp_header_start_byte)
        packet_obj.ack_number = self._get_ack_number(hex_tokens, tcp_header_start_byte)
        packet_obj.window_size = self._get_window_size(hex_tokens, tcp_header_start_byte)
        packet_obj.flags = self._get_flags(hex_tokens, tcp_header_start_byte)
        packet_obj.source_ip = self._get_source_ip(hex_tokens)
        packet_obj.dest_ip = self._get_dest_ip(hex_tokens)
        packet_obj.http_mode, packet_obj.http_header = self._get_http_mode(packet_obj.tcp_payload_len, hex_tokens)
        return packet_obj

    def _get_http_mode(self, remains, hex_tokens):
        """
        Function to get the HTTP packet type: request/response/Neither
        :param remains:
        :param hex_tokens:
        :return:
        """
        mode = None
        header_bytes = None
        if remains <= 3:
            return mode, header_bytes
        full_byte_str = ''.join(hex_tokens)
        get_start = full_byte_str.find('474554')
        resp_start = full_byte_str.find('323030204f4b')
        http_header_end = full_byte_str.find('0d0a0d0a')
        if http_header_end != -1:
            if get_start != -1:
                mode = 'req'
                header_bytes = full_byte_str[get_start:http_header_end]
            if resp_start != -1:
                mode = 'res'
                header_bytes = full_byte_str[resp_start - 18:http_header_end]
        return mode, header_bytes

    def display_packet_info(self, packet):
        print("**************************************************")
        print("FRAME NUMBER:: {0}".format(packet.frame_number))
        print("PACKET SIZE:: {0}".format(packet.packet_size))
        print("RELATIVE ARRIVAL TIME: {0}".format(packet.relative_arr_time))
        print("SOURCE IP ADDRESS:: {0}".format(packet.source_ip))
        print("DESTINATION IP ADDRESS:: {0}".format(packet.dest_ip))
        print("SOURCE PORT:: {0}".format(packet.source_port))
        print("DESTINATION PORT:: {0}".format(packet.dest_port))
        print("TCP HEADER LENGTH:: {0}".format(packet.tcp_header_len))
        print("TCP PAYLOAD LENGTH:: {0}".format(packet.tcp_payload_len))
        print("SEQUENCE NUMBER:: {0}".format(packet.seq_number))
        print("ACKNOWLEDGEMENT NUMBER:: {0}".format(packet.ack_number))
        print("WINDOW SIZE:: {0}".format(packet.window_size))
        print("FLAGS:: {0}".format(packet.flags))
        print("**************************************************")

    def _get_tcp_len(self, hex_tokens):
        frame_len = len(hex_tokens)
        ether_header_len = 14
        ip_header_info_token = hex_tokens[ether_header_len]
        ip_header_len = int(ip_header_info_token[1]) * 4
        tcp_header_start_byte = ether_header_len + ip_header_len
        tcp_header_len = 4 * int(hex_tokens[tcp_header_start_byte + 12][0], 16)
        tcp_payload_len = frame_len - ether_header_len - ip_header_len - tcp_header_len
        return tcp_header_len, tcp_payload_len

    def _get_tcp_header_start_byte(self, hex_tokens):
        ether_header_len = 14
        ip_header_info_token = hex_tokens[ether_header_len]
        ip_header_len = int(ip_header_info_token[1]) * 4
        return ether_header_len + ip_header_len

    def _serialize_packet_to_hex_tokens(self, packet):
        raw_data = codecs.encode(bytes(packet), 'hex_codec')
        raw_data = str(raw_data).split('\'')[1].split('\'')[0]
        raw_data = re.findall('..?', raw_data)
        return raw_data

    def _get_source_ip(self, hex_tokens):
        ip_addr = str(int(hex_tokens[26], 16)) + '.' + str(int(hex_tokens[27], 16)) + '.' + \
                  str(int(hex_tokens[28], 16)) + '.' + str(int(hex_tokens[29], 16))
        return ip_addr

    def _get_dest_ip(self, hex_tokens):
        ip_addr = str(int(hex_tokens[30], 16)) + '.' + str(int(hex_tokens[31], 16)) + '.' + \
                  str(int(hex_tokens[32], 16)) + '.' + str(int(hex_tokens[33], 16))
        return ip_addr

    def _get_source_port(self, hex_tokens, tcp_header_start_byte):
        str_port = hex_tokens[tcp_header_start_byte] + hex_tokens[tcp_header_start_byte + 1]
        return int(str_port, 16)

    def _get_destination_port(self, hex_tokens, tcp_header_start_byte):
        dest_port = hex_tokens[tcp_header_start_byte + 2] + hex_tokens[tcp_header_start_byte + 3]
        return int(dest_port, 16)

    def _get_sequence_number(self, hex_tokens, tcp_header_start_byte):
        seq_num = hex_tokens[tcp_header_start_byte + 4] + hex_tokens[tcp_header_start_byte + 5] + \
                  hex_tokens[tcp_header_start_byte + 6] + hex_tokens[tcp_header_start_byte + 7]
        return int(seq_num, 16)

    def _get_ack_number(self, hex_tokens, tcp_header_start_byte):
        ack_num = hex_tokens[tcp_header_start_byte + 8] + hex_tokens[tcp_header_start_byte + 9] + \
                  hex_tokens[tcp_header_start_byte + 10] + hex_tokens[tcp_header_start_byte + 11]
        return int(ack_num, 16)

    def _get_window_size(self, hex_tokens, tcp_header_start_byte):
        window_size = hex_tokens[tcp_header_start_byte + 14] + hex_tokens[tcp_header_start_byte + 15]
        return int(window_size, 16)

    def _get_flags(self, hex_tokens, tcp_header_start_byte):
        flags = []
        FIN = 0x01
        SYN = 0x02
        RST = 0x04
        PSH = 0x08
        ACK = 0x10
        URG = 0x20
        ECE = 0x40
        CWR = 0x80
        flags_hex = hex_tokens[tcp_header_start_byte + 12] + hex_tokens[tcp_header_start_byte + 13]
        flags_hex = int(flags_hex, 16)
        if flags_hex & FIN:
            flags.append('FIN')
        if flags_hex & SYN:
            flags.append('SYN')
        if flags_hex & RST:
            flags.append('RST')
        if flags_hex & PSH:
            flags.append('PSH')
        if flags_hex & ACK:
            flags.append('ACK')
        if flags_hex & URG:
            flags.append('URG')
        if flags_hex & ECE:
            flags.append('ECE')
        if flags_hex & CWR:
            flags.append('CWR')
        return flags

    def _get_flows(self, syn_requests):
        flows = []
        for i in range(0, len(syn_requests)):
            flows.append([])
            for k in range(int(syn_requests[i].frame_number) - 1, len(self.packets)):
                var1 = self.packets[k].source_port == syn_requests[i].source_port
                if var1 or self.packets[k].dest_port == syn_requests[i].source_port:
                    flows[i].append(self.packets[k])
        return flows


if __name__ == '__main__':
    # NOTE: The functions are inter dependent. Kindly not comment them individually, false results would occur
    print("Please wait for the program to execute completely! The output is huge, advised to redirect it to a file!")
    pcap_analyzer_obj = PCAPAnalyzer()
    pcap_analyzer_obj.find_total_HTTP_flows()
    pcap_analyzer_obj.find_throughput()
    pcap_analyzer_obj.estimate_loss_rate()

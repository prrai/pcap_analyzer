import logging
import sys
import re
import codecs
from collections import defaultdict
from math import ceil, sqrt

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import PcapReader

source_node_ip = '130.245.145.12'
destination_node_ip = '128.208.2.198'


class Packet:
    """
    Packet class constructed to have a structure of the header sections that are useful
    """
    def __init__(self):
        self.frame_number = ''
        self.packet_size = ''
        self.relative_arr_time = ''
        self.source_ip = ''
        self.dest_ip = ''
        self.source_port = ''
        self.dest_port = ''
        self.seq_number = ''
        self.ack_number = ''
        self.window_size = ''
        self.tcp_payload_len = ''
        self.tcp_header_len = ''
        self.mss = ''
        self.flags = []


class PCAPAnalyzer:
    """
    PCAP analyzer class that has methods to implement all the required functionaly
    """
    def __init__(self):
        self.packets = []
        self.flows = []
        self.pcap_file_path = ''
        self.loss_rates = []
        self.average_rtt = []
        self.mss_list = []
        self.empirical_throughputs = []
        self.validate_args_and_populate_packets()
        pass

    def validate_args_and_populate_packets(self):
        """
        Prints usage for wrong command line, else parses the PCAP file to fill the packet object list
        :return:
        """
        if len(sys.argv) == 2:
            if sys.argv[1] in {"--help", "--HELP", "--h", "--H", "-h", "-H", "-help", "-HELP"}:
                self._display_usage_and_exit()
            self.pcap_file_path = sys.argv[1]
            self._read_pcap_and_fill_packets()
        else:
            print("Incomplete/Improper command line arguments supplied.")
            self._display_usage_and_exit()

    def find_total_TCP_flows(self):
        """
        Function to iterate the packets and classify them into a list with packets from each flow
        self.flows[i] => List of all the packets from flow i
        :return:
        """
        syn_requests = []
        fin_requests = []
        fin_responses = []
        syn_responses = []
        for i in range(0, len(self.packets)):
            # Get all the SYN, FIN requests from sender and server side
            if ['SYN'] == self.packets[i].flags and self.packets[i].source_ip == source_node_ip:
                syn_requests.append(self.packets[i])
            if 'FIN' in self.packets[i].flags and self.packets[i].source_ip == source_node_ip:
                fin_requests.append(self.packets[i])
            if {'FIN', 'ACK'}.issubset(self.packets[i].flags) and self.packets[i].source_ip == destination_node_ip:
                fin_responses.append(self.packets[i])
            if {'SYN', 'ACK'}.issubset(self.packets[i].flags) and self.packets[i].source_ip == destination_node_ip:
                syn_responses.append(self.packets[i])
        # Display TCP flow summary
        print("Total TCP flows in the trace: {0}".format(len(syn_requests)))
        print("SYN_requests made: {0}".format(len(syn_requests)))
        print("FIN_requests made: {0}".format(len(fin_requests)))
        print("SYN_responses received: {0}".format(len(syn_responses)))
        print("FIN_responses received: {0}".format(len(fin_responses)))
        # Obtain the flows
        self.flows = self._get_flows(syn_requests)
        # Display the details of each flow
        for i in range(0, len(self.flows)):
            print("Total packets exchanged in Flow #{0} : {1}".format(i + 1, len(self.flows[i])))
        print("\n\nDisplaying details for each flow:")
        ctr = 1
        for i in range(0, len(syn_requests)):
            current_fin_response = Packet()
            current_fin_request = Packet()
            current_syn_response = Packet()
            current_syn_ack = Packet()
            current_fin_ack = Packet()
            for j in range(0, len(fin_responses)):
                if syn_requests[i].source_port == fin_responses[j].dest_port:
                    if syn_requests[i].dest_port == fin_responses[j].source_port:
                        if syn_requests[i].source_ip == fin_responses[j].dest_ip:
                            if syn_requests[i].dest_ip == fin_responses[j].source_ip:
                                current_fin_response = fin_responses[j]
                                break

            for j in range(0, len(syn_responses)):
                if syn_requests[i].source_port == syn_responses[j].dest_port:
                    if syn_requests[i].dest_port == syn_responses[j].source_port:
                        if syn_requests[i].source_ip == syn_responses[j].dest_ip:
                            if syn_requests[i].dest_ip == syn_responses[j].source_ip:
                                current_syn_response = syn_responses[j]
                                break

            for k in range(int(current_fin_response.frame_number) - 1, len(self.packets)):
                if ['ACK'] == self.packets[k].flags and self.packets[k].source_ip == source_node_ip:
                    if self.packets[k].dest_ip == destination_node_ip:
                        if self.packets[k].source_port == current_fin_response.dest_port:
                            current_fin_ack = self.packets[k]
                            break

            for k in range(int(current_syn_response.frame_number) - 1, len(self.packets)):
                if ['ACK'] == self.packets[k].flags and self.packets[k].source_ip == source_node_ip:
                    if self.packets[k].dest_ip == destination_node_ip:
                        if self.packets[k].source_port == current_syn_response.dest_port:
                            current_syn_ack = self.packets[k]
                            break

            for j in range(0, len(fin_requests)):
                if syn_requests[i].source_port == fin_requests[j].source_port:
                    if syn_requests[i].dest_port == fin_requests[j].dest_port:
                        if syn_requests[i].source_ip == fin_requests[j].source_ip:
                            if syn_requests[i].dest_ip == fin_requests[j].dest_ip:
                                current_fin_request = fin_requests[j]
                                break
            # Flow display
            print("\n============== TCP FLOW #{0}: BEGIN ================".format(ctr))
            print("\n-------------THREE WAY HANDSHAKE: BEGIN-----------")
            print("SYN REQUEST PACKET FROM SOURCE TO DESTINATION:")
            self.display_packet_info(syn_requests[i])
            print("\nSYN-ACK RESPONSE PACKET FROM DESTINATION TO SOURCE:")
            self.display_packet_info(current_syn_response)
            print("\nACK RESPONSE PACKET FROM SOURCE TO DESTINATION:")
            self.display_packet_info(current_syn_ack)
            print("-------------THREE WAY HANDSHAKE: END-------------")
            print("\n-----------CONNECTION TEAR DOWN: BEGIN------------")
            print("FIN REQUEST PACKET FROM SOURCE TO DESTINATION:")
            self.display_packet_info(current_fin_request)
            print("\nFIN-ACK RESPONSE PACKET FROM DESTINATION TO SOURCE:")
            self.display_packet_info(current_fin_response)
            print("\nACK RESPONSE PACKET FROM SOURCE TO DESTINATION:")
            self.display_packet_info(current_fin_ack)
            print("-------------CONNECTION TEAR DOWN: END------------")
            print("=============== TCP FLOW #{0}: END =================".format(ctr))
            ctr += 1
            # Display first two transactions for each flow
            self._display_first_two_transactions_per_flow(current_syn_ack, i)

    def estimate_loss_rate(self):
        """
        Function to estimate loss rate for each flow
        :return:
        """
        print("\n======= LOSS RATE CALCULATIONS PER FLOW =========")
        for i in range(0, len(self.flows)):
            sent_count = 0
            print("Estimating loss rate for TCP FLOW# {0}".format(i + 1))
            print("**************************************************")
            sender_port = self.flows[i][0].source_port
            print("Losses => Retransmitted packets since original ones didn't get acknowledged in time")
            sender_retrans_dict = defaultdict(int)
            for k in range(0, len(self.flows[i])):
                # Filter all sent packets and create map of frequency of (seq + payload_len)
                if self.flows[i][k].source_port == sender_port:
                    sender_retrans_dict[(self.flows[i][k].seq_number, self.flows[i][k].tcp_payload_len,
                                         self.flows[i][k].ack_number)] += 1
                    sent_count += 1
            retransmits = 0
            for key in sender_retrans_dict:
                # Sum up all the frequencies
                retransmits += sender_retrans_dict[key]
            # Delete the sum from total unique (seq + payload_len). This will give us the total number of duplicates
            retransmits -= len(sender_retrans_dict)
            print("Total retransmissions are: {0}".format(retransmits))
            print("Total sent packets from source are: {0}".format(sent_count))
            print("Loss rate = (retransmits / sent packets)")
            print("_________________________________________________________________________")
            print("Loss rate % = {0}".format(100 * round(float(retransmits / sent_count), 6)))
            self.loss_rates.append(round(float(retransmits / sent_count), 6))
            print("_________________________________________________________________________")
            print("**************************************************\n")

    def find_empirical_throughput(self):
        """
        Function to estimate empirical throughput
        """
        print("\n======= EMPIRICAL THROUGHPUT CALCULATIONS (SOURCE TO DESTINATION) PER FLOW =========")
        for i in range(0, len(self.flows)):
            print("Estimating throughput for TCP FLOW# {0}".format(i + 1))
            print("**************************************************")
            bytes_sent_to_receiver = 0
            first_source_packet = self.flows[i][0]
            for packet in self.flows[i]:
                # Sum up all the bytes sent from sender
                if packet.source_port == first_source_packet.source_port:
                    bytes_sent_to_receiver += packet.packet_size
            bytes_sent_to_receiver *= 8
            print("Total Bits sent for FLOW {0}, from sender: {1} on port: {2} is: {3}"
                  .format(i + 1, first_source_packet.source_ip, first_source_packet.source_port,
                          bytes_sent_to_receiver))
            print("Total time spent = Handshake Time + Time spent in data exchange (all sent bytes acknowledged) "
                  "+ Tear down time")
            # Get the total time spent in sending all the bytes
            time_spent = round(float(self.flows[i][-1].relative_arr_time) -
                               float(self.flows[i][0].relative_arr_time), 6)
            print("Total Time: {0}".format(time_spent))
            print("Empirical throughput = (Total Bits sent) / (Total Time)")
            throughput = round(float(bytes_sent_to_receiver / time_spent), 6)
            # Use the relation below to get throughput
            self.empirical_throughputs.append(float(bytes_sent_to_receiver / time_spent))
            kbps = round(float(throughput / 8 / 1024), 6)
            print("_________________________________________________________________________")
            print("Hence, Throughput = {0} Bits/sec => {1} Kilobytes/sec".format(throughput, kbps))
            print("_________________________________________________________________________")
            print("**************************************************\n")

    def get_congestion_windows(self):
        """
        Function to get congestion window sizes, first 5.
        Can be controlled by increasing the iteration of loop.
        :return:
        """
        print("=========================== WINDOW SIZE PER FLOW : BEGIN ============================")
        print("\n\n***************************** INITIAL WINDOW SIZE ESTIMATION *************************")
        print("Logic: After the handshake, the window size  is a factor of MSS\n")
        print("We calculate the total bytes transferred by the sender before starting to wait for the ACK,\n")
        print("since that those are the maximum bytes it could send.\n")
        print("We divide those bytes by the MSS for send and take the ceiling function on it to get the factor.\n")
        print("We also need to consider the receiver window size since the max bytes that could be sent is\n")
        print("equal to the minimum of icwnd and rwnd.\n")
        print("\n***************************** INITIAL WINDOW SIZE ESTIMATION *************************\n\n")
        for i in range(len(self.flows)):
            print("***************************** INITIAL WINDOW SIZE ESTIMATION: FLOW: {0} : BEGIN **"
                  "***********************".format(i + 1))
            bytes_transferred_so_far = 0
            x = 0
            seq_seen = set()
            sender_dict = defaultdict()
            print(
                "Starting with the first packet after hand shake, get stats for each packet exchanged until first ACK")
            for j in range(3, len(self.flows[i])):
                # Get the first ACK packet and break
                if self.flows[i][j].source_port == self.flows[i][0].dest_port:
                    if self.flows[i][j].ack_number in sender_dict:
                        print("Found ACK, Frame number: {0}, ACK number: {1}, corresponding source "
                              "packet's frame number: "
                              "{2}".format(self.flows[i][j].frame_number,
                                           self.flows[i][j].ack_number,
                                           sender_dict[self.flows[i][j].ack_number].frame_number))
                        x = j
                        break
                # Otherwise keep pn accumulating the bytes transferred
                if self.flows[i][j].source_port == self.flows[i][0].source_port:
                    sender_dict[(self.flows[i][j].seq_number + self.flows[i][j].tcp_payload_len)] = self.flows[i][j]
                    bytes_transferred_so_far += self.flows[i][j].tcp_payload_len
                    seq_seen.add(self.flows[i][j].seq_number)
                    print("Sending source packet: Frame number: {0}, Seq number: {1}, Total Bytes before ACK: {2}".
                          format(self.flows[i][j].frame_number, self.flows[i][j].seq_number, bytes_transferred_so_far))
            print("\n Total bytes transferred by sender initially: {0}".format(bytes_transferred_so_far))
            print("MSS value for sender: {0}".format(self.flows[i][0].mss))
            print("Receiver's MSS value: {0}".format(self.flows[i][1].mss))
            # MSS is the minimum of sender vs receiver MSS
            agreed_mss = min(self.flows[i][0].mss, self.flows[i][1].mss)
            self.mss_list.append(agreed_mss)
            print("Minimum of the two to get agreed MSS: {0}".format(agreed_mss))
            print("Receiver intial window size (rwnd): {0}".format(self.flows[i][1].window_size))
            # Get bounds on ICWND
            if bytes_transferred_so_far <= self.flows[i][1].window_size:
                init_win_fact = ceil(float(bytes_transferred_so_far / agreed_mss))
                print("ICWND => ceil[ (total bytes) / agreed MSS ] : {0}".format(init_win_fact))
                print("Since the total bytes transferred is lesser than\n"
                      "receiver window size, we can conclude that icwnd\n"
                      "is an upper bound of total bytes sent, i.e.: total_bytes <= icwnd < rwnd.\n")
                print("Hence our estimation of the factor of {0} is accurate.".format(init_win_fact))
                print("________________________________________________________________________________________")
                init_win_sz = init_win_fact * agreed_mss
                print("Initial congestion window size = {0} x Sender's MSS = {0} x {1} = {2} Bytes".
                      format(init_win_fact, agreed_mss, init_win_sz))
                print("_________________________________________________________________________________________")
            else:
                print("Since the total bytes transferred is higher than\n"
                      "receiver window size, we can conclude that rwnd\n"
                      "is an upper bound of total bytes sent, i.e.: total_bytes <= rwnd < icwnd.\n")
                print("Hence, icwnd can be any value greater than rwnd. Here, we will estimate it as "
                      "the lowest step above MSS greater than rwcd\n")
                init_win_fact = ceil(float(self.flows[i][1].window_size / agreed_mss))
                print("ICWND => ceil[ rcwd / agreed MSS ] : {0}".format(init_win_fact))
                print("________________________________________________________________________________________")
                init_win_sz = init_win_fact * agreed_mss
                print("Initial congestion window size = {0} x Sender's MSS = {0} x {1} = {2} Bytes".
                      format(init_win_fact, agreed_mss, init_win_sz))
                print("_________________________________________________________________________________________")
                print("******************** INITIAL WINDOW SIZE ESTIMATION: FLOW: {0} : END ***********"
                      "***********************\n\n".format(i + 1))
            # Get next 4 window sizes using the logic below
            print("************** NEXT 4 CONGESTION WINDOWS FOR FLOW: {0} : BEGIN **************".format(i + 1))
            print("Logic => Keep track of syns sent, keep increasing icwnd estimated \n"
                  "above by a factor of MSS whenever an ACK is received. \n"
                  "Once a duplicate syn is being sent, it indicates a loss: Reduce the windows size \n"
                  "back to icwnd and start incrementing again\n")
            win_size = init_win_sz
            ctr = 0
            for j in range(x + 1, len(self.flows[i])):
                if ctr == 4:
                    break
                if self.flows[i][j].source_port == self.flows[0][0].source_port:
                    if self.flows[i][j].seq_number in seq_seen:
                        print("Duplicate Sequence number observed, indicates a loss, reduce congestion window to icwd")
                        print("Previous congestion window = {0}, Current congestion window = {1}".
                              format(win_size, init_win_sz))
                        ctr += 1
                        win_size = init_win_sz
                    else:
                        seq_seen.add(self.flows[i][j].seq_number)
                if self.flows[i][j].source_port == self.flows[0][0].dest_port:
                    print("ACK received from server side, no loss since last RTT, increase window factor by 1")
                    print("Previous congestion window = {0}, Current congestion window = {1}".
                          format(win_size, win_size + agreed_mss))
                    win_size += agreed_mss
                    ctr += 1
            print("************** NEXT 4 CONGESTION WINDOWS FOR FLOW: {0} : END **************".format(i + 1))
        print("===========================  WINDOW SIZE PER FLOW : END ============================")

    def get_retransmission_distribution(self):
        """
        Get the number of packets retransmitted due to triple dup ACK vs timeouts
        :return:
        """
        print("\n\n=========================== RETRANSMISSION DISTRIBUTION PER FLOW : BEGIN ================="
              "===========")
        for i in range(0, len(self.flows)):
            print("***************************** RETRANSMISSION ESTIMATION: FLOW: {0} : BEGIN **"
                  "***********************".format(i + 1))
            print("\nGet all the triple duplicate ACKs from the receiver side..")
            ack_freq = defaultdict()
            for j in range(0, len(self.flows[i])):
                # Build the frequencies of acks
                if self.flows[i][j].source_port == self.flows[i][0].dest_port:
                    if self.flows[i][j].ack_number in ack_freq:
                        ack_freq[self.flows[i][j].ack_number] += 1
                    else:
                        ack_freq[self.flows[i][j].ack_number] = 1
            for key in ack_freq:
                ack_freq[key] -= 1
                # Filter out triple dup ACKs
                if ack_freq[key] > 2:
                    print("Duplicate ACK number: {0} received {1} times.".format(key, ack_freq[key]))
            seq_freq = defaultdict()
            print("\nGet all the retransmitted packets from server side..")
            triple_retrans = 0
            time_out_retrans = 0
            for j in range(0, len(self.flows[i])):
                # Build freqencies of tuple of seq_num and payload_len
                if self.flows[i][j].source_port == self.flows[i][0].source_port:
                    if (self.flows[i][j].seq_number, self.flows[i][j].tcp_payload_len) in seq_freq:
                        seq_freq[(self.flows[i][j].seq_number, self.flows[i][j].tcp_payload_len)] += 1
                    else:
                        seq_freq[(self.flows[i][j].seq_number, self.flows[i][j].tcp_payload_len)] = 1
            for key in seq_freq:
                # For all retransmitted packets, look up in the dictionary for triple dup acks
                if seq_freq[key] > 1:
                    print("Duplicate SEQ number: {0} sent {1} times.".format(key, seq_freq[key]))
                    print("Look up {0} in triple duplicate ACK frequency table..".format(key[0]))
                    curr_seq = key[0]
                    if curr_seq in ack_freq and ack_freq[curr_seq] > 2:
                        triple_retrans += 1
                        print("Found {0}, this retransmission was due to triple "
                              "duplicate ACK. Total such retransmissions so far: {1}".format(curr_seq, triple_retrans))
                    else:
                        # Else classify them as timed out
                        time_out_retrans += 1
                        print("Could not find {0}, this retransmission was due to timeout "
                              "Total such retransmissions so far: {1}".format(curr_seq, time_out_retrans))
            print("__________________________________________________________________________")
            print("SUMMARY:")
            print("Total retransmissions in the flow: {0}".format(time_out_retrans + triple_retrans))
            print("Retransmissions due to triple duplicate ACK reception: {0}".format(triple_retrans))
            print("Retransmissions due to timeout: {0}".format(time_out_retrans))
            print("__________________________________________________________________________")
            print("***************************** RETRANSMISSION ESTIMATION: FLOW: {0} : END **"
                  "***********************".format(i + 1))
        print("=========================== RETRANSMISSION DISTRIBUTION PER FLOW : END ============================")

    def estimate_avg_RTT(self):
        """
        Average RTT estimation
        :return:
        """
        print("\n======= AVERAGE RTT CALCULATIONS (SOURCE) PER FLOW =========")
        for i in range(0, len(self.flows)):
            print("Estimating AVERAGE RTT for TCP FLOW# {0}".format(i + 1))
            print("**************************************************")
            seq_ack_dict = defaultdict()
            ack_dict = defaultdict()
            source_port = self.flows[i][0].source_port
            count = 0
            rtt_sum = 0
            for j in range(0, len(self.flows[i])):
                # For each sent request, record its arrival time, retransmitted ones will get over-written
                if self.flows[i][j].source_port == source_port:
                    if ('SYN' or 'FIN') in self.flows[i][j].flags:
                        # If the packet was a SYN or FIN, the ACK number would have incremented by 1, handle that case.
                        seq_ack_dict[self.flows[i][j].seq_number +
                                     self.flows[i][j].tcp_payload_len + 1] = self.flows[i][j].relative_arr_time
                    else:
                        seq_ack_dict[self.flows[i][j].seq_number +
                                     self.flows[i][j].tcp_payload_len] = self.flows[i][j].relative_arr_time
            for j in range(0, len(self.flows[i])):
                # Record all the ack numbers, dup ones will get over-written
                if self.flows[i][j].dest_port == source_port:
                    ack_dict[self.flows[i][j].ack_number] = self.flows[i][j].relative_arr_time
            for key in seq_ack_dict:
                # Check if (seq_num + len) from map1 is present in map2. If yes, take it as a sample
                if key in ack_dict:
                    # Accumulate RTT sum and sample size
                    rtt_sum += ack_dict[key] - seq_ack_dict[key]
                    count += 1
            print("RTT sum: {0}".format(rtt_sum))
            print("Total packets: {0}".format(count))
            print("Average RTT is: {0}".format(float(rtt_sum) / float(count)))
            print("**************************************************")
            self.average_rtt.append(float(rtt_sum) / float(count))

    def compare_empirical_vs_theoretical_throughputs(self):
        """
        Function to compare empirical vs theoretical throughputs
        :return:
        """
        print("\n======= THEORETICAL VS EMPIRICAL THROUGHPUT CALCULATIONS (SOURCE TO DESTINATION) PER FLOW : BEGIN "
              "=========")
        print("Throughput (if p > 0) = ((MSS) / (RTT)) * sqrt(3 / (2 * p))")
        print("Throughput (if p = 0) = ((RWND) / (RTT)))")
        for i in range(0, len(self.flows)):
            print("\nEstimating theoretical throughput for TCP FLOW# {0}".format(i + 1))
            print("********************************************************")
            # Traverse these values from DS already created during estimation of Loss, RTT and IW
            print("MSS = {0} bytes".format(self.mss_list[i]))
            print("RTT = {0} seconds".format(self.average_rtt[i]))
            print("LOSS RATE(p) = {0}".format(self.loss_rates[i]))
            if self.loss_rates[i] > 0:
                res = float(self.mss_list[i] / self.average_rtt[i])
                res *= sqrt(float(3 / 2))
                res /= sqrt(float(self.loss_rates[i]))
                print("Theoretical throughput = ((MSS) / (RTT)) * sqrt(3 / (2 * p)) = {0} bytes/sec".format(res))
            else:
                # Use the approximation below for loss rate of 0
                print("Receiver window size: {0} bytes".format(self.flows[i][1].window_size))
                res = float(self.flows[i][1].window_size / self.average_rtt[i])
                print("Theoretical throughput = ((RWND) / (RTT)) = {0} bytes/sec".format(res))
            print("********************************************************")
            print("_________________________________________________________")
            print("SUMMARY:")
            print("Theoretical throughput = {0} bytes/sec".format(res))
            print("Empirical throughput = {0} bytes/sec".format(float(self.empirical_throughputs[i] / 8)))
            print("Ratio of empirical throughput and theoretical throughput = {0}"
                  .format(float(self.empirical_throughputs[i] / 8 / res)))
            print("_________________________________________________________")
        print("\n======= THEORETICAL VS EMPIRICAL THROUGHPUT CALCULATIONS (SOURCE TO DESTINATION) PER FLOW : END"
              " =========")

    # Protected methods

    def _display_usage_and_exit(self):
        """
        Help menu
        :return:
        """
        print("\n[USAGE]: python3 analysis_pcap_tcp.py <path to pcap file>")
        print("[EXAMPLE]: python3 analysis_pcap_tcp.py <assignment2.pcap>")
        sys.exit(2)

    def _read_pcap_and_fill_packets(self):
        """
        Function to read PCAP file and fill all the fields in Packet() for each such packet in the file
        :return:
        """
        try:
            with PcapReader(self.pcap_file_path) as pcap_data:
                time_ref = 0
                for packet in pcap_data:
                    if len(self.packets) == 0:
                        time_ref = packet.time
                    hex_tokens = self._serialize_packet_to_hex_tokens(packet)
                    packet_obj = Packet()
                    packet_obj.packet_size = len(packet)
                    packet_obj.frame_number = int(len(self.packets) + 1)
                    packet_obj.relative_arr_time = round(float(packet.time - time_ref), 6)
                    packet_obj = self._extract_packet_info(packet_obj, hex_tokens)
                    self.packets.append(packet_obj)
        except EnvironmentError:
            print("\n[ERROR] Unable to read input file, or it does not exist. Aborting!\n")
            self._display_usage_and_exit()

    def _extract_packet_info(self, packet_obj, hex_tokens):
        """
        Extract packet information for each packet, individual fields have separate methods
        :param packet_obj:
        :param hex_tokens:
        :return:
        """
        tcp_header_start_byte = self._get_tcp_header_start_byte(hex_tokens)
        packet_obj.tcp_header_len, packet_obj.tcp_payload_len = self._get_tcp_len(hex_tokens)
        packet_obj.source_port = self._get_source_port(hex_tokens, tcp_header_start_byte)
        packet_obj.dest_port = self._get_destination_port(hex_tokens, tcp_header_start_byte)
        packet_obj.seq_number = self._get_sequence_number(hex_tokens, tcp_header_start_byte)
        packet_obj.ack_number = self._get_ack_number(hex_tokens, tcp_header_start_byte)
        packet_obj.window_size = self._get_window_size(hex_tokens, tcp_header_start_byte)
        packet_obj.flags = self._get_flags(hex_tokens, tcp_header_start_byte)
        packet_obj.source_ip = self._get_source_ip(hex_tokens)
        packet_obj.mss = self._get_mss(hex_tokens, tcp_header_start_byte)
        packet_obj.dest_ip = self._get_dest_ip(hex_tokens)
        return packet_obj

    def _display_first_two_transactions_per_flow(self, current_syn_ack, i):
        """
        Function to display the first two transactions after handshake
        :param current_syn_ack:
        :param i:
        :return:
        """
        print("\n\nDisplay first transaction for flow# {4} between \n"
              "SOURCE(IP: {0}, PORT: {1}) "
              "and \nDESTINATION(IP: {2}, PORT: {3})"
              .format(current_syn_ack.source_ip, current_syn_ack.source_port,
                      current_syn_ack.dest_ip, current_syn_ack.dest_port, i + 1))
        print("========== TRANSACTION #1: BEGIN =================")
        print("PACKET FROM SOURCE TO DESTINATION: ")
        source_packet = Packet()
        destination_packet = Packet()
        source_packet_num = 0
        dest_packet_num = 0
        for j in range(0, len(self.flows[i])):
            if self.flows[i][j] == current_syn_ack:
                source_packet = self.flows[i][j + 1]
                source_packet_num = j + 1
                break
        self.display_packet_info(source_packet)

        for k in range(source_packet_num - 1, len(self.flows[i])):
            if self.flows[i][k].dest_port == source_packet.source_port:
                if self.flows[i][k].ack_number == int(source_packet.tcp_payload_len) + int(source_packet.seq_number):
                    destination_packet = self.flows[i][k]
                    dest_packet_num = k
                    break
        print("ACK PACKET FROM DESTINATION TO SOURCE:")
        self.display_packet_info(destination_packet)
        print("RTT to acknowledge the packet: {0}"
              .format(round(float(destination_packet.relative_arr_time) - float(source_packet.relative_arr_time), 6)))
        print("Additional packets send from \n"
              "source to destination \non this flow before ACK sent: {0}"
              .format(dest_packet_num - source_packet_num - 1))
        print("=================TRANSACTION #1: END==============")
        print("\nDisplay second transaction for flow# {4} between \n"
              "SOURCE(IP: {0}, PORT: {1}) "
              "and \nDESTINATION(IP: {2}, PORT: {3}"
              .format(current_syn_ack.source_ip, current_syn_ack.source_port,
                      current_syn_ack.dest_ip, current_syn_ack.dest_port, i + 1))
        print("============= TRANSACTION #2: BEGIN ==============")
        print("PACKET FROM SOURCE TO DESTINATION: ")
        source_packet = self.flows[i][source_packet_num + 1]
        source_packet_num += 1
        self.display_packet_info(source_packet)

        for k in range(source_packet_num - 1, len(self.flows[i])):
            if self.flows[i][k].dest_port == source_packet.source_port:
                if self.flows[i][k].ack_number == int(source_packet.tcp_payload_len) + \
                        int(source_packet.seq_number):
                    destination_packet = self.flows[i][k]
                    dest_packet_num = k
                    break
        print("ACK PACKET FROM DESTINATION TO SOURCE:")
        self.display_packet_info(destination_packet)
        print("RTT to acknowledge the packet: {0}"
              .format(round(float(destination_packet.relative_arr_time) - float(source_packet.relative_arr_time), 6)))
        print("Additional packets send from \n"
              "source to destination \non this flow before ACK sent: {0}"
              .format(dest_packet_num - source_packet_num - 1))
        print("===============TRANSACTION #2: END================")

    def display_packet_info(self, packet):
        """
        Function to display packet fields
        :param packet:
        :return:
        """
        print("**************************************************")
        print("FRAME NUMBER:: {0}".format(packet.frame_number))
        print("PACKET SIZE:: {0}".format(packet.packet_size))
        print("RELATIVE ARRIVAL TIME: {0}".format(packet.relative_arr_time))
        print("SOURCE IP ADDRESS:: {0}".format(packet.source_ip))
        print("DESTINATION IP ADDRESS:: {0}".format(packet.dest_ip))
        print("SOURCE PORT:: {0}".format(packet.source_port))
        print("DESTINATION PORT:: {0}".format(packet.dest_port))
        print("TCP HEADER LENGTH:: {0}".format(packet.tcp_header_len))
        print("TCP PAYLOAD LENGTH:: {0}".format(packet.tcp_payload_len))
        print("SEQUENCE NUMBER:: {0}".format(packet.seq_number))
        print("ACKNOWLEDGEMENT NUMBER:: {0}".format(packet.ack_number))
        print("WINDOW SIZE:: {0}".format(packet.window_size))
        print("FLAGS:: {0}".format(packet.flags))
        print("**************************************************")

    def _get_tcp_len(self, hex_tokens):
        """
        Function to get length of various headers and TCP payload
        :param hex_tokens:
        :return:
        """
        frame_len = len(hex_tokens)
        ether_header_len = 14
        ip_header_info_token = hex_tokens[ether_header_len]
        ip_header_len = int(ip_header_info_token[1]) * 4
        tcp_header_start_byte = ether_header_len + ip_header_len
        tcp_header_len = 4 * int(hex_tokens[tcp_header_start_byte + 12][0], 16)
        tcp_payload_len = frame_len - ether_header_len - ip_header_len - tcp_header_len
        return tcp_header_len, tcp_payload_len

    def _get_mss(self, hex_tokens, tcp_header_start_byte):
        """
        Function to parse options part and get MSS
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        is_mss = True if int(hex_tokens[tcp_header_start_byte + 20], 16) is 2 else False
        if is_mss is False:
            return 536  # per https://tools.ietf.org/html/rfc2581#ref-Bra89
        mss = hex_tokens[tcp_header_start_byte + 22] + hex_tokens[tcp_header_start_byte + 23]
        return int(mss, 16)

    def _get_tcp_header_start_byte(self, hex_tokens):
        """
        Get the start byte of tcp header
        :param hex_tokens:
        :return:
        """
        ether_header_len = 14
        ip_header_info_token = hex_tokens[ether_header_len]
        ip_header_len = int(ip_header_info_token[1]) * 4
        return ether_header_len + ip_header_len

    def _serialize_packet_to_hex_tokens(self, packet):
        """
        Serialize the packet bytes to hex stream
        :param packet:
        :return:
        """
        raw_data = codecs.encode(bytes(packet), 'hex_codec')
        raw_data = str(raw_data).split('\'')[1].split('\'')[0]
        raw_data = re.findall('..?', raw_data)
        return raw_data

    def _get_source_ip(self, hex_tokens):
        """
        Function to get source IP from IP header
        :param hex_tokens:
        :return:
        """
        ip_addr = str(int(hex_tokens[26], 16)) + '.' + str(int(hex_tokens[27], 16)) + '.' + \
                  str(int(hex_tokens[28], 16)) + '.' + str(int(hex_tokens[29], 16))
        return ip_addr

    def _get_dest_ip(self, hex_tokens):
        """
        Function to get destination IP from IP header
        :param hex_tokens:
        :return:
        """
        ip_addr = str(int(hex_tokens[30], 16)) + '.' + str(int(hex_tokens[31], 16)) + '.' + \
                  str(int(hex_tokens[32], 16)) + '.' + str(int(hex_tokens[33], 16))
        return ip_addr

    def _get_source_port(self, hex_tokens, tcp_header_start_byte):
        """
        Get source port
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        str_port = hex_tokens[tcp_header_start_byte] + hex_tokens[tcp_header_start_byte + 1]
        return int(str_port, 16)

    def _get_destination_port(self, hex_tokens, tcp_header_start_byte):
        """
        Get destination port from TCP header
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        dest_port = hex_tokens[tcp_header_start_byte + 2] + hex_tokens[tcp_header_start_byte + 3]
        return int(dest_port, 16)

    def _get_sequence_number(self, hex_tokens, tcp_header_start_byte):
        """
        Get sequence number from TCP header
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        seq_num = hex_tokens[tcp_header_start_byte + 4] + hex_tokens[tcp_header_start_byte + 5] + \
                  hex_tokens[tcp_header_start_byte + 6] + hex_tokens[tcp_header_start_byte + 7]
        return int(seq_num, 16)

    def _get_ack_number(self, hex_tokens, tcp_header_start_byte):
        """
        Get ACK number from TCP header
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        ack_num = hex_tokens[tcp_header_start_byte + 8] + hex_tokens[tcp_header_start_byte + 9] + \
                  hex_tokens[tcp_header_start_byte + 10] + hex_tokens[tcp_header_start_byte + 11]
        return int(ack_num, 16)

    def _get_window_size(self, hex_tokens, tcp_header_start_byte):
        """
        Get TCP window size (not the congestion window, the flow control capacity window)
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        window_size = hex_tokens[tcp_header_start_byte + 14] + hex_tokens[tcp_header_start_byte + 15]
        return int(window_size, 16)

    def _get_flags(self, hex_tokens, tcp_header_start_byte):
        """
        Function to parse the flag field and extract individual flags that are ON
        :param hex_tokens:
        :param tcp_header_start_byte:
        :return:
        """
        flags = []
        FIN = 0x01
        SYN = 0x02
        RST = 0x04
        PSH = 0x08
        ACK = 0x10
        URG = 0x20
        ECE = 0x40
        CWR = 0x80
        flags_hex = hex_tokens[tcp_header_start_byte + 12] + hex_tokens[tcp_header_start_byte + 13]
        flags_hex = int(flags_hex, 16)
        if flags_hex & FIN:
            flags.append('FIN')
        if flags_hex & SYN:
            flags.append('SYN')
        if flags_hex & RST:
            flags.append('RST')
        if flags_hex & PSH:
            flags.append('PSH')
        if flags_hex & ACK:
            flags.append('ACK')
        if flags_hex & URG:
            flags.append('URG')
        if flags_hex & ECE:
            flags.append('ECE')
        if flags_hex & CWR:
            flags.append('CWR')
        return flags

    def _get_flows(self, syn_requests):
        """
        Function to get flows
        :param syn_requests:
        :return:
        """
        flows = []
        for i in range(0, len(syn_requests)):
            flows.append([])
            for k in range(int(syn_requests[i].frame_number) - 1, len(self.packets)):
                var1 = self.packets[k].source_port == syn_requests[i].source_port
                if var1 or self.packets[k].dest_port == syn_requests[i].source_port:
                    flows[i].append(self.packets[k])
        return flows


if __name__ == '__main__':
    # NOTE: The functions are inter dependent. Kindly not comment them individually, false results would occur
    print("Please wait for the program to execute completely! The output is huge, advised to redirect it to a file!")
    pcap_analyzer_obj = PCAPAnalyzer()
    pcap_analyzer_obj.find_total_TCP_flows()
    pcap_analyzer_obj.find_empirical_throughput()
    pcap_analyzer_obj.estimate_loss_rate()
    pcap_analyzer_obj.estimate_avg_RTT()
    pcap_analyzer_obj.get_congestion_windows()
    pcap_analyzer_obj.compare_empirical_vs_theoretical_throughputs()
    pcap_analyzer_obj.get_retransmission_distribution()
